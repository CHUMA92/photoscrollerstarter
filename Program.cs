﻿using System;
using System.Windows.Forms;

namespace PhotoScroller
{
    /// <summary>
    /// Entry point into the photo scroller application
    /// </summary>
    public static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        public static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Application.Run(new PhotoScroller());
        }
    }
}
