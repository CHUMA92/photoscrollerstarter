﻿using System;
using System.IO;
using System.Linq;
using System.Windows.Forms;
using PhotoScroller.Model;

namespace PhotoScroller
{
    /// <summary>
    /// Photo scroller Code-behind file
    /// </summary>
    public partial class PhotoScroller : Form
    {
        private readonly PictureCollection collection;

        /// <summary>
        /// Initializes a new instance of the <see cref="PhotoScroller" /> class.
        /// </summary>
        public PhotoScroller()
        {
            this.InitializeComponent();
            this.collection = new PictureCollection();
            const string initialTime = "0";
            if (this.timerBox.Text == initialTime)
            {
                this.timer.Stop();
            }
        }

        /// <summary>
        /// Handles the CheckedChanged event of the radioButton control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
        private void radioButton_CheckedChanged(object sender, EventArgs e)
        {
            this.updateCheckBoxControls(sender);

            if (this.pictureBox.Image == null)
            {
                return;
            }
            this.checkRadioState();
        }

        /// <summary>
        /// Handles the Click event of the buttonAddPhoto control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
        private void buttonAddPhoto_Click(object sender, EventArgs e)
        {
            using (var dlg = new OpenFileDialog())
            {
                dlg.Multiselect = true;
                dlg.Title = @"Open Image";
                dlg.Filter = @"Images (*.BMP;*.JPG;*.GIF;*.PNG;)|*.BMP;*.JPG;*.GIF;*.PNG|" + @"All files (*.*)|*.*";

                if (dlg.ShowDialog() != DialogResult.OK)
                {
                    return;
                }

                foreach (var file in dlg.FileNames)
                {
                    try
                    {
                        this.collection.AddPhoto(file, this.pictureBox);

                        this.checkRadioBoxState();
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show(ex.Message);
                    }
                }
            }
        }

        /// <summary>
        /// Handles the Click event of the buttonAddFolder control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
        private void buttonAddFolder_Click(object sender, EventArgs e)
        {
            using (var fbd = new FolderBrowserDialog())
                try
                {
                    {
                        if (fbd.ShowDialog() != DialogResult.OK)
                        {
                            return;
                        }
                        foreach (var file in Directory.GetFiles(fbd.SelectedPath, "*.*", SearchOption.AllDirectories).Where(s =>
                        {
                            const string supportedExtensions = "*.jpg,*.gif,*.png,*.bmp,*.jpe,*.jpeg,*.tif,*.tiff";
                            var extension = Path.GetExtension(s);
                            return extension != null && supportedExtensions.Contains(extension.ToLower());
                        }))
                        {
                            var extension = Path.GetExtension(file);
                            if (extension != null && extension.Length > 4)
                            {
                                continue;
                            }

                            this.collection.AddPhoto(file, this.pictureBox);

                            this.checkRadioBoxState();
                        }
                    }
                }
                catch (Exception ex)
                {

                    MessageBox.Show(ex.Message);
                }
        }

        /// <summary>
        /// Handles the Click event of the buttonForward control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
        private void buttonForward_Click(object sender, EventArgs e)
        {
            if (this.pictureBox.Image == null)
            {
                return;
            }
            this.collection.GetNextImage(this.pictureBox);
            this.checkRadioBoxState();
        }

        /// <summary>
        /// Handles the Click event of the buttonBack control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
        private void buttonBack_Click(object sender, EventArgs e)
        {
            if (this.pictureBox.Image == null)
            {
                return;
            }
            this.collection.GetLastImage(this.pictureBox);
            this.checkRadioBoxState();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void checkBoxGreen_CheckedChanged(object sender, EventArgs e)
        {
            if (this.pictureBox.Image == null)
            {
                return;
            }
            this.checkBoxStates();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void checkBoxRed_CheckedChanged(object sender, EventArgs e)
        {
            if (this.pictureBox.Image == null)
            {
                return;
            }
            this.checkBoxStates();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void checkBoxBlue_CheckedChanged(object sender, EventArgs e)
        {
            if (this.pictureBox.Image == null)
            {
                return;
            }
            this.checkBoxStates();
        }

        /// <summary>
        /// Handles the Tick event of the timer control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
        private void timer_Tick(object sender, EventArgs e)
        {
            if (this.pictureBox.Image == null)
            {
                return;
            }
            this.collection.GetNextImage(this.pictureBox);
            this.checkRadioBoxState();
        }

        /// <summary>
        /// Handles the Click event of the stopButton control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
        private void stopButton_Click(object sender, EventArgs e)
        {
            if (this.pictureBox.Image == null)
            {
                return;
            }
            this.checkRadioBoxState();
            this.timer.Stop();
        }

        /// <summary>
        /// Handles the Click event of the startButton control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
        private void startButton_Click(object sender, EventArgs e)
        {
            if (this.pictureBox.Image == null)
            {
                return;
            }
            try
            {
                this.checkRadioBoxState();
                this.startTimer();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                this.timer.Stop();
            }
        }

        #region Helper Methods

        /// <summary>
        /// Updates the CheckBox controls.
        /// </summary>
        /// <param name="sender">The sender.</param>
        private void updateCheckBoxControls(object sender)
        {
            if (sender == this.radioButtonColor)
            {
                this.enableCheckBoxes(true);
            }
            else
            {
                this.enableCheckBoxes(false);
            }
        }

        /// <summary>
        /// Enables the check boxes.
        /// </summary>
        /// <param name="status">if set to <c>true</c> [status].</param>
        private void enableCheckBoxes(bool status)
        {
            this.checkBoxRed.Enabled = status;
            this.checkBoxGreen.Enabled = status;
            this.checkBoxBlue.Enabled = status;
        }

        /// <summary>
        /// Starts the timer.
        /// </summary>
        private void startTimer()
        {
            this.timer.Start();
            this.timer.Enabled = true;
            var time = Convert.ToInt32(this.timerBox.Text);
            this.timer.Interval = time;
        }

        /// <summary>
        /// Checks the state of the radio box.
        /// </summary>
        private void checkRadioBoxState()
        {
            this.checkRadioState();
            if (this.radioButtonColor.Checked)
            {
                this.checkBoxStates();
            }
        }

        /// <summary>
        /// Checks the checkbox states.
        /// </summary>
        private void checkBoxStates()
        {
            if (this.checkBoxRed.CheckState == CheckState.Unchecked)
            {
                this.collection.PictureFilter.BlueGreenScaleFilter(this.pictureBox);
            }
            if (this.checkBoxGreen.CheckState == CheckState.Unchecked)
            {
                this.collection.PictureFilter.RedBlueScaleFilter(this.pictureBox);
            }
            if (this.checkBoxBlue.CheckState == CheckState.Unchecked)
            {
                this.collection.PictureFilter.RedGreenScaleFilter(this.pictureBox);
            }



            if (this.checkBoxRed.Checked && !this.checkBoxGreen.Checked && !this.checkBoxBlue.Checked)
            {
                this.collection.PictureFilter.RedScaleFilter(this.pictureBox);
            }
            if (!this.checkBoxRed.Checked && !this.checkBoxGreen.Checked && this.checkBoxBlue.Checked)
            {
                this.collection.PictureFilter.BlueScaleFilter(this.pictureBox);
            }
            if (!this.checkBoxRed.Checked && this.checkBoxGreen.Checked && !this.checkBoxBlue.Checked)
            {
                this.collection.PictureFilter.GreenScaleFilter(this.pictureBox);
            }
            if (!this.checkBoxRed.Checked && !this.checkBoxGreen.Checked && !this.checkBoxBlue.Checked)
            {
                this.collection.PictureFilter.NoFilter(this.pictureBox);
            }
            if (this.checkBoxRed.Checked && this.checkBoxGreen.Checked && this.checkBoxBlue.Checked)
            {
                this.collection.PictureFilter.CloneImage(this.pictureBox);
            }
        }

        /// <summary>
        /// Checks the state of the radio button.
        /// </summary>
        private void checkRadioState()
        {
            if (this.radioButtonGray.Checked)
            {
                this.collection.PictureFilter.GrayScaleFilter(this.pictureBox);
            }
            else if (this.radioButtonSepiaTone.Checked)
            {
                this.collection.PictureFilter.SepiaToneFilter(this.pictureBox);
            }
            else if (this.radioButtonColor.Checked)
            {
                this.collection.PictureFilter.CloneImage(this.pictureBox);
            }
        }

        #endregion

    }
}