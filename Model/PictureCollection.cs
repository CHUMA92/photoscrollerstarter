﻿using System.Collections.Generic;
using System.Drawing;
using System.Windows.Forms;

namespace PhotoScroller.Model
{
    /// <summary>
    /// Picture Collection represent a collection of image objects class
    /// </summary>
    public class PictureCollection
    {
        #region Instance Variables

        private readonly PictureFilter pictureFilter;

        #endregion

        #region Properties

        /// <summary>
        /// Gets or sets the loaded images.
        /// </summary>
        /// <value>
        /// The loaded images.
        /// </value>
        public List<Image> LoadedImages { get; set; }

        /// <summary>
        /// Gets or sets the index of the current image.
        /// </summary>
        /// <value>
        /// The index of the current image.
        /// </value>
        public int CurrentImageIndex { get; set; }

        /// <summary>
        /// Gets the picture filter.
        /// </summary>
        /// <value>
        /// The picture filter.
        /// </value>
        public PictureFilter PictureFilter
        {
            get { return this.pictureFilter; }
        }

        #endregion

        #region Constructor

        /// <summary>
        /// Initializes a new instance of the <see cref="T:System.Object"/> class.
        /// </summary>
        public PictureCollection()
        {
            this.LoadedImages = new List<Image>();
            this.pictureFilter = new PictureFilter(this);
        }

        #endregion

        #region Methods

        /// <summary>
        /// Gets the last image.
        /// </summary>
        /// <param name="box">The box.</param>
        public void GetLastImage(PictureBox box)
        {
            this.CurrentImageIndex = mod(this.CurrentImageIndex - 1, this.LoadedImages.Count);
            box.Image = this.LoadedImages[this.CurrentImageIndex];
        }

        /// <summary>
        /// Gets the next image.
        /// </summary>
        /// <param name="box">The box.</param>
        public void GetNextImage(PictureBox box)
        {
            this.CurrentImageIndex = mod(this.CurrentImageIndex + 1, this.LoadedImages.Count);
            box.Image = this.LoadedImages[this.CurrentImageIndex];
        }

        /// <summary>
        /// Adds the photo.
        /// </summary>
        /// <param name="file">The file.</param>
        /// <param name="box">The box.</param>
        public void AddPhoto(string file, PictureBox box)
        {
            this.LoadedImages.Add(Image.FromFile(file));

            if (this.LoadedImages.Count <= 0)
            {
                return;
            }
            this.CurrentImageIndex = 0;
            box.Image = this.LoadedImages[this.CurrentImageIndex];
        }

        #region Helper Method

        /// <summary>
        /// Mods the specified number.
        /// </summary>
        /// <param name="a">a.</param>
        /// <param name="b">The b.</param>
        /// <returns>System.Int32.</returns>
        private static int mod(int a, int b)
        {
            return (a % b + b) % b;
        }

        #endregion

        #endregion
    }
}
