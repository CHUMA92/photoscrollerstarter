using System;
using System.Drawing;
using System.Drawing.Imaging;
using System.Windows.Forms;

namespace PhotoScroller.Model
{
    /// <summary>
    /// Picture Filter represents the filtered image
    /// </summary>
    public class PictureFilter
    {
        #region Instance Variable

        private readonly PictureCollection pictureCollection;

        #endregion

        #region Constructor

        /// <summary>
        /// Initializes a new instance of the <see cref="PictureFilter"/> class.
        /// </summary>
        /// <param name="pictureCollection">The picture collection.</param>
        public PictureFilter(PictureCollection pictureCollection)
        {
            this.pictureCollection = pictureCollection;
        }

        #endregion

        #region Methods

        /// <summary>
        /// Grays the scale filter.
        /// </summary>
        /// <param name="box">The box.</param>
        public void GrayScaleFilter(PictureBox box)
        {
            var matrix = new ColorMatrix(new[]
            {
                new[] {0.299f, 0.299f, 0.299f, 0, 0},
                new[] {0.587f, 0.587f, 0.587f, 0, 0},
                new[] {0.114f, 0.114f, 0.114f, 0, 0},
                new float[] {0, 0, 0, 1, 0},
                new float[] {0, 0, 0, 0, 0}
            });
            this.applyColorMatrix(matrix, box);
        }

        /// <summary>
        /// Reds the scale filter.
        /// </summary>
        /// <param name="box">The box.</param>
        public void RedBlueScaleFilter(PictureBox box)
        {
            var matrix = new ColorMatrix(new[]
            {
                new float[] {1, 0, 0, 0, 0},
                new float[] {0, 0, 0, 0, 0},
                new float[] {0, 0, 1, 0, 0},
                new float[] {0, 0, 0, 1, 0},
                new float[] {0, 0, 0, 0, 1}
            });
            this.applyColorMatrix(matrix, box);
        }

        /// <summary>
        /// Greens the scale filter.
        /// </summary>
        /// <param name="box">The box.</param>
        public void RedGreenScaleFilter(PictureBox box)
        {
            var matrix = new ColorMatrix(new[]
            {
                new float[] {1, 0, 0, 0, 0},
                new float[] {0, 1, 0, 0, 0},
                new float[] {0, 0, 0, 0, 0},
                new float[] {0, 0, 0, 1, 0},
                new float[] {0, 0, 0, 0, 1}
            });
            this.applyColorMatrix(matrix, box);
        }

        /// <summary>
        /// Blues the scale filter.
        /// </summary>
        /// <param name="box">The box.</param>
        public void BlueGreenScaleFilter(PictureBox box)
        {
            var matrix = new ColorMatrix(new[]
            {
                new float[] {0, 0, 0, 0, 0},
                new float[] {0, 1, 0, 0, 0},
                new float[] {0, 0, 1, 0, 0},
                new float[] {0, 0, 0, 1, 0},
                new float[] {0, 0, 0, 0, 1}
            });
            this.applyColorMatrix(matrix, box);
        }

        /// <summary>
        /// Reds the scale filter.
        /// </summary>
        /// <param name="box">The box.</param>
        public void RedScaleFilter(PictureBox box)
        {
            var matrix = new ColorMatrix(new[]
            {
                new float[] {1, 0, 0, 0, 0},
                new float[] {0, 0, 0, 0, 0},
                new float[] {0, 0, 0, 0, 0},
                new float[] {0, 0, 0, 1, 0},
                new float[] {0, 0, 0, 0, 1}
            });
            this.applyColorMatrix(matrix, box);
        }

        /// <summary>
        /// Blues the scale filter.
        /// </summary>
        /// <param name="box">The box.</param>
        public void BlueScaleFilter(PictureBox box)
        {
            var matrix = new ColorMatrix(new[]
            {
                new float[] {0, 0, 0, 0, 0},
                new float[] {0, 0, 0, 0, 0},
                new float[] {0, 0, 1, 0, 0},
                new float[] {0, 0, 0, 1, 0},
                new float[] {0, 0, 0, 0, 1}
            });
            this.applyColorMatrix(matrix, box);
        }

        /// <summary>
        /// Blues the scale filter.
        /// </summary>
        /// <param name="box">The box.</param>
        public void GreenScaleFilter(PictureBox box)
        {
            var matrix = new ColorMatrix(new[]
            {
                new float[] {0, 0, 0, 0, 0},
                new float[] {0, 1, 0, 0, 0},
                new float[] {0, 0, 0, 0, 0},
                new float[] {0, 0, 0, 1, 0},
                new float[] {0, 0, 0, 0, 1}
            });
            this.applyColorMatrix(matrix, box);
        }

        /// <summary>
        /// Applys no filter.
        /// </summary>
        /// <param name="box">The box.</param>
        public void NoFilter(PictureBox box)
        {
            var matrix = new ColorMatrix(new[]
            {
                new float[] {0, 0, 0, 0, 0},
                new float[] {0, 0, 0, 0, 0},
                new float[] {0, 0, 0, 0, 0},
                new float[] {0, 0, 0, 1, 0},
                new float[] {0, 0, 0, 0, 1}
            });
            this.applyColorMatrix(matrix, box);
        }

        /// <summary>
        /// Sepias the tone filter.
        /// </summary>
        /// <param name="box">The box.</param>
        public void SepiaToneFilter(PictureBox box)
        {
            var matrix = new ColorMatrix(new[]
            {
                new[]{.393f, .349f, .272f, 0, 0},
                new[]{.769f, .686f, .534f, 0, 0},
                new[]{.189f, .168f, .131f, 0, 0},
                new float[]{0, 0, 0, 1, 0},
                new float[]{0, 0, 0, 0, 1}
            });

            this.applyColorMatrix(matrix, box);
        }

        /// <summary>
        /// Clones the image.
        /// </summary>
        /// <param name="box">The box.</param>
        public void CloneImage(PictureBox box)
        {
            if (box.Image == null)
            {
                return;
            }
            try
            {
                box.Image = (Image)this.pictureCollection.LoadedImages[this.pictureCollection.CurrentImageIndex].Clone();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        #region Helper Method

        /// <summary>
        /// Applies the color matrix.
        /// </summary>
        /// <param name="matrix">The matrix.</param>
        /// <param name="box">The box.</param>
        private void applyColorMatrix(ColorMatrix matrix, PictureBox box)
        {
            try
            {
                var loadImages = this.pictureCollection.LoadedImages;
                var currentImage = Math.Abs(this.pictureCollection.CurrentImageIndex);
                var updateImage = (Image)loadImages[currentImage].Clone();
                box.Image = updateImage;
                var image = box.Image;

                var attributes = new ImageAttributes();

                attributes.SetColorMatrix(matrix);

                var graphics = Graphics.FromImage(image);

                graphics.DrawImage(image,
                    new Rectangle(0, 0, image.Width, image.Height), 0, 0, image.Width, image.Height, GraphicsUnit.Pixel, attributes);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        #endregion
        #endregion
    }
}